import java.util.Arrays;

public class ClosestNeighbours {

        public static void main(String[] args) {

            int[] myArray = {0, 5, 1209, 6, 2, 111, 112, 33};
            int[] result = findClosest(myArray);
            System.out.println(Arrays.toString(result));
            // print the elements of the resulting array
        }

        private static int[] findClosest(int[] numbers) {
            int previousDifference = -1;

            int[] closestNeighbours = new int[2];

            for(int n = 0; n < numbers.length; n++) {
                if(n == 7) {
                    break;
                }

                int difference = Math.abs(numbers[n] - numbers[n + 1]);

                int currentDifference = difference;

                if(currentDifference < previousDifference || previousDifference == -1) {
                    previousDifference = currentDifference;

                    closestNeighbours[0] = numbers[n];
                    closestNeighbours[1] = numbers[n + 1];
                    System.out.println(Arrays.toString(closestNeighbours));
                }
            }

            return closestNeighbours;
        }

}
